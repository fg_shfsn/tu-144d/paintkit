#!/bin/sh --
set -e

# FSX to FlightGear livery converter for Tu-144D.
#
# REQUIREMENTS
# 1. NVIDIA Texture Tools
# 2. ImageMagick

# PNG compression args.
IMFLAGS='-define png:compression-level=9'

# Create workdir.
mkdir work
trap "rm -rf work" 0 2 9 15

# List of textures that we're going to convert.
ATLAS='fuse1 fuse2 fuse3 fuse4 intake_r tail titan_1 titan_2 wing2 wing3 wing5'
ATLAS_NONMAP='interior noseg salon shassi'

# Copy all textures to work dir.
for texture in $ATLAS; do
 cp -i "$texture"_t.dds work/
 cp -i "$texture"_bump.dds work/
done
for texture in $ATLAS_NONMAP; do
 cp -i "$texture"_t.dds work/
done

cd work

# Convert diffuse textures to png.
for texture in $ATLAS $ATLAS_NONMAP; do
 (
  name="$texture"_t
  nvdecompress "$name".dds
  # Flip and discard alpha.
  convert -flip -alpha off "$name".tga "$name".png
  rm -f "$name".tga
 )
done

# Convert normal maps to png, convert from FSX encoding.
for texture in $ATLAS; do
 (
  name="$texture"_bump
  [ -e "$name".dds ] || return
  nvdecompress "$name".dds
  # Flip.
  convert -flip "$name".tga "$name".png
  rm -f "$name".tga
  # FSX normals documentation:
  # https://www.fsdeveloper.com/wiki/index.php?title=Normal_map_creation#Saving_in_FSX_format
  # Swap red and alpha channels, invert green, discard alpha.
  mogrify -channel rgba -separate -swap 0,3 -combine +channel "$name".png
  mogrify -channel green -negate +channel -alpha off "$name".png
 )
done

# Create placeholder images.
convert -size 2048x2048 xc:'#000000' dummy_t.png
convert -size 2048x2048 xc:'#8080ff' dummy_bump.png

wait

# Dummy out missing normalmaps.
for texture in $ATLAS_NONMAP; do
 cp dummy_bump.png "${texture}"_bump.png
done

# Combine Tu-144D textures.
for type in t bump; do
 convert $IMFLAGS\
  \(\
   fuse4_"$type".png\
   fuse3_"$type".png\
   fuse2_"$type".png\
   fuse1_"$type".png\
   -resize 2048x2048 +append\
  \)\
  \(\
   wing5_"$type".png\
   \(\
    \(\
     titan_2_"$type".png\
     titan_1_"$type".png\
     -resize 2048x2048 +append\
    \)\
    \(\
     noseg_"$type".png\
     shassi_"$type".png\
     -resize 2048x2048 +append\
    \)\
    -append\
   \)\
   -resize 4096x4096 +append\
  \)\
  \(\
   tail_"$type".png\
   dummy_"$type".png\
   \(\
    \(\
     dummy_"$type".png\
     dummy_"$type".png\
     -resize 1024x1024 +append\
    \)\
    \(\
     dummy_"$type".png\
     wing3_"$type".png\
     -resize 1024x1024 +append\
    \)\
    -append\
   \)\
   \(\
    \(\
     wing2_"$type".png\
     interior_"$type".png\
     -resize 1024x1024 +append\
    \)\
    \(\
     intake_r_"$type".png\
     salon_"$type".png\
     -resize 1024x1024 +append\
    \)\
    -append\
   \)\
   -resize 2048x2048 +append\
  \)\
  -append\
  atlas_"$type".png
done

# Output own textures for pushback and stairs.
convert $IMFLAGS wing2_t.png ../BelAZ-74212_d.png
convert $IMFLAGS wing2_bump.png ../BelAZ-74212_n.png
convert $IMFLAGS intake_r_t.png ../SPT-154_d.png
convert $IMFLAGS intake_r_bump.png ../SPT-154_n.png

wait

mv atlas_t.png ../Tu-144D_d.png
mv atlas_bump.png ../Tu-144D_n.png

cd ..
