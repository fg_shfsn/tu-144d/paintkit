#!/bin/sh -- 
set -e

mainrepo=${1:?}

livery=${2:?}

if [ "$livery" == "Aeroflot_USSR-77115.svg" ]; then
 # Default livery is stored directly in Models.
 out="$mainrepo/Models/Tu-144D_d.dds"
else
 out="$mainrepo/Models/Liveries/$(basename -s .svg $livery).dds"
fi
if [ "$livery" == "paintkit.svg" ]; then
 echo Not building the paintkit. If you still want to build it, run >&2
 echo bin/makedds_d.sh '"'$livery'"' '"'$out'"' >&2
 echo manually. >&2
 exit 1
fi
bin/makedds_d.sh "$livery" "$out"
