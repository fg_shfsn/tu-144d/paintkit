#!/bin/sh --
set -e

in=${1:?}
out=${2:?}

# BEGIN XXX ImageMagick convert shits itself when it sees relative paths in SVG, so we have to use Inkscape and then convert to tga.
#convert -background black -alpha remove -flip "$in" "$out-makeddstemp.tga"
inkscape --export-type=png --export-overwrite --export-filename="$out-makeddstemp.png" "$in"
convert -background black -alpha remove -flip "$out-makeddstemp.png" "$out-makeddstemp.tga"
rm -f "$out-makeddstemp.png"
# END XXX
nvcompress -color -bc1 "$out-makeddstemp.tga"
mv "$out-makeddstemp.dds" "$out"
rm -f "$out-makeddstemp.tga"
