#!/bin/sh --
set -e

JOBS=${JOBS:-1}

mainrepo=${1:?}
shift

echo ${@:?} |xargs -n1 -P$JOBS bin/makelivery.sh "$mainrepo"
